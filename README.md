# IPMI Fanspeed Daemon

This daemon uses ipmitool to override the default fanspeed control on Dell servers. 
It has only been tested on 11th generation Dell servers (specifically the R410), 
your mileage may vary on other generations or other platforms. 

# Dependencies

`python-psutil` `python-daemon` `ipmitool`