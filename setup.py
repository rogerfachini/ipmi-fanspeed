import os
from setuptools import setup
setup(
    name = "ipmifanspeed",
    version = "0.0.8",
    author = "Roger Fachini",
    author_email = "r3fachini@live.com",
    description = ("Daemon for controlling Dell IPMI fanspeeds from coretemp temperatures"),
    license = "BSD",
    keywords = "ipmi idrac coretemp fanspeed",
    url = "https://gitlab.com/rogerfachini/ipmi-fanspeed",
    packages=['ipmifanspeed'],
    entry_points={
        'console_scripts': [
            'ipmi-fanspeed = ipmifanspeed.ipmi_fanspeed:main'
        ]
    },
    long_description="",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
