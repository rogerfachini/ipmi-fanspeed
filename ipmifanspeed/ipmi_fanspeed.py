# ipmi-fanspeed: Daemon for controlling Dell IPMI fanspeeds from coretemp temperatures
# requires (apt packages):
# python-psutil, ipmitool, python-daemon

import psutil
import daemon

import time
import os
import argparse
import subprocess
import sys
import lockfile
import signal

#------Constants------
MIN_SPEED = 17
MAX_SPEED = 100

MAX_TEMP = 60
MIN_TEMP = 35

DEBUG = True

#------Global Variables------

LastSpeed = None

def run_cmd(cmd):
    os.system(cmd)

def disable_bios_fanspeed():
    run_cmd('/usr/bin/ipmitool raw 0x30 0x30 0x01 0x00')

def enable_bios_fanspeed():
    run_cmd('/usr/bin/ipmitool raw 0x30 0x30 0x01 0x01')

def set_raw_speed(speed):
    if speed > MAX_SPEED:
        speed = MAX_SPEED
    if speed < MIN_SPEED:
        speed = MIN_SPEED #0x11 min, 0x64 max
    run_cmd('/usr/bin/ipmitool raw 0x30 0x30 0x02 0xff %s' % hex(speed))

def set_speed(percent):
    global LastSpeed
    if percent < 0:
        percent = 0
    if percent > 1:
        percent = 1
    if LastSpeed == percent:
        return
    else:
        LastSpeed = percent
    spd = (percent * (100 - MIN_SPEED)) + MIN_SPEED
    set_raw_speed(int(spd))

def temp_control_loop():
    run = True
    while run:
        temps = psutil.sensors_temperatures().get('coretemp')
        current_temps = [t.current for t in temps]
        avg_temp = sum(current_temps) / float(len(current_temps))

        tper = (avg_temp - MIN_TEMP) / (MAX_TEMP - MIN_TEMP)
        tper = round(tper, 2)

        set_speed(tper)
        if DEBUG:
            print(avg_temp, tper)
        time.sleep(0.25)

def check_environment():
    if not hasattr(psutil, 'sensors_temperatures'):
        raise RuntimeError('psutil does not support sensors_temperatures! Upgrade your psutil version.')

    temps = psutil.sensors_temperatures()
    if not temps:
        raise RuntimeError('psutil does not detect any temperature sensors! Make sure lm-sensors is installed, and run sensors-detect to detect sensors in your system.')

    if not temps.get('coretemp'):
        raise RuntimeError('psutil detected sensors but did not find coretemp. Detected %s. Report this to the script developer!' % ','.join(temps.keys()) )

    if not os.path.isfile('/usr/bin/ipmitool'):
        raise RuntimeError('ipmitool not detected. Make sure ipmitool is installed.')

def run_fan_control():
    try:
        check_environment()
        disable_bios_fanspeed()                                                                                                                                                                                                   
        temp_control_loop()                                                                                                                                                                                                      
    except RuntimeError as err:                                                                                                                                                                                                      
        print('Error: %s' % err)
        exit(1)
    except KeyboardInterrupt:
        exit(0)                                                                                                                                                                             
    finally:                                                                                                                                                                                                                              
        enable_bios_fanspeed()

def shutdown(signum, frame):
    """Safe shutdown when process is killed"""
    enable_bios_fanspeed()
    if os.path.isfile('/var/run/ipmifanspeed.pid'):
         os.remove('/var/run/ipmifanspeed.pid')
    if os.path.isfile('/var/run/ipmifanspeed.pid.lock'):
         os.remove('/var/run/ipmifanspeed.pid.lock')
    exit(0)

def kill():
    """Send sigterm to process specified in PID file"""
    try:
        f = open('/var/run/ipmifanspeed.pid', 'r')
        pid = int(f.read())
        f.close()
        os.kill(pid, signal.SIGTERM)
    except IOError:
        print('PIDfile does not exist')
    shutdown(None, None)
    return pid

def main():
    global DEBUG
    parser = argparse.ArgumentParser(description='Control fanspeeds on a Dell server using ipmitool.')
    parser.add_argument('-d', '--daemon', action='store_true', help='Daemonize (fork, etc) the process')
    parser.add_argument('-v', '--debug', action='store_true', help='Print debug information to stdout')
    parser.add_argument('-k', '--kill', action='store_true', help='Kill the currently running daemon')
    parser.add_argument('-r', '--reload', action='store_true', help='Kill the currently running daemon and start')
    args = parser.parse_args()
    DEBUG = args.debug

    pidfile = lockfile.FileLock('/var/run/ipmifanspeed.pid')

    if args.kill and not pidfile.is_locked():
        print('No processes to kill, exiting')
        exit(1)

    if args.kill and pidfile.is_locked():
        pid = kill()
        print('Sent SIGTERM to pid %s' % pid)
        exit(0)

    if pidfile.is_locked() and not args.reload:
        print('ipmi-fanspeed daemon is already running!')
        exit(1)

    if args.reload:
        pid = kill()
        args.daemon = True

    if args.daemon:
        sigmap = { signal.SIGTERM: shutdown,
                   signal.SIGTSTP: shutdown}
        if args.debug:
            dContext = daemon.DaemonContext(signal_map=sigmap, pidfile=pidfile, uid=0, gid=0, stdout=sys.stdout, stderr=sys.stderr)
        else:
            dContext = daemon.DaemonContext(signal_map=sigmap, pidfile=pidfile, uid=0, gid=0)
        with dContext:
            pid = str(os.getpid())
            f = open('/var/run/ipmifanspeed.pid', 'w')
            f.write(pid)
            f.close()
            run_fan_control()
    else:
        signal.signal(signal.SIGTERM, shutdown)
        run_fan_control()


if __name__ == '__main__':
    main()
